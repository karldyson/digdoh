#!/usr/bin/env perl

use strict;
use LWP;
use Net::DNS;

# set some defaults
my $class = 'IN';
my %opt = ( 'rd' => 1, 'cd' => 0, 'do' => 0 );
my $provider;
my $params;

# need to be able to convert qtype ID back to qtype string, and can't find a module method to do that
# so there's this hack in the mean time
my @qtypes;
$qtypes[1] = 'A';
$qtypes[2] = 'NS';
$qtypes[5] = 'CNAME';
$qtypes[6] = 'SOA';
$qtypes[12] = 'PTR';
$qtypes[15] = 'MX';
$qtypes[16] = 'TXT';
$qtypes[28] = 'AAAA';
$qtypes[33] = 'SRV';
$qtypes[43] = 'DS';
$qtypes[44] = 'SSHFP';
$qtypes[46] = 'RRSIG';
$qtypes[47] = 'NSEC';
$qtypes[48] = 'DNSKEY';
$qtypes[50] = 'NSEC3';
$qtypes[51] = 'NSEC3PARAM';
$qtypes[52] = 'TLSA';
$qtypes[59] = 'CDS';
$qtypes[60] = 'CDNSKEY';
$qtypes[61] = 'OPENPGPKEY';
$qtypes[99] = 'SPF';
$qtypes[255] = 'ANY';

# scoot through the command line args looking for things
for my $argv (@ARGV) {
	if(grep /^$argv$/i, @qtypes) {
		$params->{QTYPE} = uc($argv);
	}
	elsif($argv =~ m/^IN|CH|HS$/i) {
		$class = $argv;
	}
	elsif($argv =~ m/^([+-])(cd|do|rd)$/) {
		$opt{$2} = $1 eq '+' ? 1 : 0;
	}
	elsif($argv =~ m/^\@(.*)$/) {
		$provider = $1;
	}
	else {
		$params->{QNAME} = $argv;
	}
}

$params->{QNAME} ||= '.';
$params->{QTYPE} ||= 'A';
die "no provider specified\n" unless $provider;

# initialise the useragent object
my $ua = LWP::UserAgent->new;
$ua->env_proxy;
$ua->timeout(5);

# determime the URL to call
my $url = "https://$provider/dns-query";

my $queryQuestion = new Net::DNS::Question($params->{QNAME}, $params->{QTYPE}, $class);
my $queryPacket = new Net::DNS::Packet;
$queryPacket->push(question => $queryQuestion);
$queryPacket->header->rd($opt{rd});
$queryPacket->header->cd($opt{cd});
$queryPacket->header->do($opt{do});

print "Query:\n";
$queryPacket->print;
print "Response:\n";

my $request = HTTP::Request->new(POST => $url);
$request->header('Content-Type' => 'application/dns-message');
$request->content($queryPacket->data());

# make the call to the API
my $response = $ua->request($request);

# if the API call was a success, build a DNS packet to print out (gives an output something close to dig with minimal effort)
if($response->is_success) {
	my $replyPacket = new Net::DNS::Packet(\$response->decoded_content);

	$replyPacket->print;

	print "Provider ....: $provider\n";
	print "Question size: ".length($queryPacket->data)." bytes\n";
	print "Response size: ".length($replyPacket->data)." bytes\n";
} else {
	die "Fatal. API said: ".$response->status_line."\n";
}

