# README #

This code is a basic implementation of a dig like interface for DNS over HTTPS endpoints.

Currently there are three scripts:

## digdohget ##

Implements the GET method of RFC8484, albeit at the moment with no HTTP/2 support.

## digdohpost ##

Implements the POST method of RFC8484, again, currently without HTTP/2 support.

## digdohgetjson ##

The name might be giving this away, heh, and this implements the JSON interface offered by Google, Cloudflare and others.

At the moment, it requires the provider to be configured in the script because there's no standard for the URI that I'm aware of at the time of writing.

## Waranty ##

There is, of course, no warranty with this. It's written for my purposes, mostly mucking about with, understanding and/or troubleshooting DNS related things. It's not my fault if it eats your cat or scares your children.
